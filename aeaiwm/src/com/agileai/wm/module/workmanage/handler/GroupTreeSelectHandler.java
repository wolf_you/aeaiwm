package com.agileai.wm.module.workmanage.handler;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.wm.module.workmanage.service.GroupTreeSelect;

public class GroupTreeSelectHandler
        extends TreeSelectHandler {
    public GroupTreeSelectHandler() {
        super();
        this.serviceId = buildServiceId(GroupTreeSelect.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "GRP_ID",
                                                  "GRP_NAME", "GRP_PID");
        String rootId = param.get("Root");
        treeBuilder.setRootId(rootId);

        return treeBuilder;
    }

    protected GroupTreeSelect getService() {
        return (GroupTreeSelect) this.lookupService(this.getServiceId());
    }
}
