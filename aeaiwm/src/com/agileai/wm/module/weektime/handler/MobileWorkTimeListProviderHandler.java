package com.agileai.wm.module.weektime.handler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.wm.module.weektime.service.WmWeektimeManage;

public class MobileWorkTimeListProviderHandler extends SimpleHandler{
	private static String ERROR = "error";
	private static String NEGATIVE = "negative";
	public MobileWorkTimeListProviderHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param){
		String responseText = FAIL;
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findWorkTimeListInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
			JSONObject jsonObject = new JSONObject();
			List<DataRow> weektimeRecords = wmWeektimeManage.findWeekTimeRecords();
			JSONArray jsonArray = new JSONArray();
			if(weektimeRecords.size() != 0){
				for(int i=0;i<weektimeRecords.size();i++){
					DataRow row = weektimeRecords.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("num", i+1);
					jsonObject1.put("id", row.stringValue("WT_ID"));
					jsonObject1.put("startTime", row.stringValue("WT_BEGIN"));
					jsonObject1.put("endTime", row.stringValue("WT_END"));
					jsonObject1.put("standDay", row.stringValue("WT_STAND_DAY"));
					jsonArray.put(jsonObject1);
				}
			}else{
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("num", 1);
				jsonObject1.put("startTime", "无");
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("weekTime", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer creteWorkTimeListInfo(DataParam param){
		String responseText = FAIL;
		try {
        	String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
    		String stimeUTC = jsonObject.get("stime").toString().replace("Z", " UTC");
    		Date sd = format.parse(stimeUTC);
    		String stime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, sd);
    		
    		SimpleDateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
    		String etimeUTC = jsonObject.get("etime").toString().replace("Z", " UTC");
    		Date ed = endFormat.parse(etimeUTC);
    		String etime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, ed);
    		
    		long diff = ed.getTime() - sd.getTime();
    		long days = diff / (1000 * 60 * 60 * 24);
    		String standDay = String.valueOf(days+1);
    		
    		WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
    		
        	if(days < 0){
        		responseText = NEGATIVE;
        	}else if(days>=0 && days<= 10){
            	wmWeektimeManage.creteWorkTimeListInfo(stime,etime,standDay);
            	responseText = SUCCESS;
        	}else{
        		responseText = ERROR;
        	}
        	
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer delWeekTimeInfo(DataParam param){
		String responseText = FAIL;
		try {
        	String id = param.get("currentId");
        	if(!id.isEmpty()){
        		WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
        		wmWeektimeManage.delWeekTimeRecord(id);
            	responseText = SUCCESS;
        	}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getWeekTimeInfo(DataParam param){
		String responseText = FAIL;
		try {
        	String weekTimeId = param.get("currentId");
        	if(!weekTimeId.isEmpty()){
        		WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
            	DataRow weekTimeRow = wmWeektimeManage.getWeekTimeInfo(weekTimeId);
            	JSONObject jsonObject = new JSONObject();
            	jsonObject.put("stime", weekTimeRow.stringValue("WT_BEGIN").substring(0, 10));
            	jsonObject.put("etime", weekTimeRow.stringValue("WT_END").substring(0, 10));
            	jsonObject.put("standDay", weekTimeRow.stringValue("WT_STAND_DAY"));
            	responseText = jsonObject.toString();
        	}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer updateWeekTimeRecord(DataParam param){
		String responseText = FAIL;
		try {
        	String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	String id = jsonObject.get("id").toString();
        	
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
    		String stimeUTC = jsonObject.get("stime").toString().replace("Z", " UTC");
    		Date sd = format.parse(stimeUTC);
    		String stime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, sd);
    		
    		SimpleDateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
    		String etimeUTC = jsonObject.get("etime").toString().replace("Z", " UTC");
    		Date ed = endFormat.parse(etimeUTC);
    		String etime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, ed);
    		
    		long diff = ed.getTime() - sd.getTime();
    		long days = diff / (1000 * 60 * 60 * 24);
    		String standDay = String.valueOf(days+1);
    		
        	WmWeektimeManage wmWeektimeManage = this.lookupService(WmWeektimeManage.class);
        	if(days < 0){
        		responseText = NEGATIVE;
        	}else if(days>0 && days<= 10){
        		wmWeektimeManage.updateWeekWorkRecord(id,stime,etime,standDay);
            	responseText = SUCCESS;
        	}else{
        		responseText = ERROR;
        	}
        	
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer calculateStandDay(DataParam param){
		String responseText = FAIL;
		try {
			
			SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss z", Locale.ENGLISH);
			String stimeUTC = param.get("stime");
			stimeUTC = stimeUTC.substring(0,33);
			stimeUTC = stimeUTC.replaceAll("0800", "+08:00");
			Date date = sf.parse(stimeUTC); 
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
    		String stime = sdf.format(date); 
			
    		
    		SimpleDateFormat endFormat = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss z", Locale.ENGLISH);
    		String etimeUTC = param.get("etime");
    		etimeUTC = etimeUTC.substring(0,33);
    		etimeUTC = etimeUTC.replaceAll("0800", "+08:00");
    		Date ed = endFormat.parse(etimeUTC);
    		String etime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, ed);
    		
    		long diff = ed.getTime() - date.getTime();
    		long days = diff / (1000 * 60 * 60 * 24);
    		String standDay = String.valueOf(days+1);
    		
    		JSONObject jsonObject = new JSONObject();
    		jsonObject.put("stime", stime.substring(0, 10));
    		jsonObject.put("etime", etime.substring(0, 10));
    		jsonObject.put("standDay", standDay);
        	responseText = jsonObject.toString();
        	
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
	
}
	
