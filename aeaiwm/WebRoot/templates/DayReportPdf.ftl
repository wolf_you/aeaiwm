<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<style>
body{
	font-family: SimSun;
}
h2{
	mso-style-link:"标题 2 Char";
	margin-top:13.0pt;
	margin-right:0cm;
	margin-bottom:13.0pt;
	margin-left:0cm;
	text-align:justify;
	text-justify:inter-ideograph;
	line-height:173%;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family: SimSun;
}
p.MsoNormal, li.MsoNormal, div.MsoNormal{
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-justify:inter-ideograph;
	font-size:10.5pt;
	font-family: SimSun;
}

WordSection1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	layout-grid:15.6pt;}
div.WordSection1
	{page:WordSection1;}

</style>
</head>
<body>
<table width="700" border="0" style="table-layout:fixed;word-break:break-strict;">
<#list model as log>
  <tr>
    <h2><b>${log.date}</b></h2> 
  </tr>
  <#list log.contentList as content>
  <tr>
    <td>
	<p class="MsoNormal">${content.text}</p><br/>  
    </td>
  </tr>
  </#list>
</#list>
</table>
</body>
</html>
